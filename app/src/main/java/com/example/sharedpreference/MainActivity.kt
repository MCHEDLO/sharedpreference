package com.example.sharedpreference

import android.content.SharedPreferences
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatViewInflater
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {


    private lateinit var sharedPreferences: SharedPreferences

     override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
         init()

    }
     fun read(view: View){
        val Email = sharedPreferences.getString("email", "")
        val name = sharedPreferences.getString("firstname", "")
        val lastname = sharedPreferences.getString("lastname", "")
        val Age = sharedPreferences.getString("age", "")
        val Address = sharedPreferences.getString("address", "")

        email.setText(Email)
        firstname.setText(name)
        surname.setText(lastname)
        age.setText(Age)
        address.setText(Address)

    }

    private fun init(){
        sharedPreferences = getSharedPreferences("userData", MODE_PRIVATE )
    }

    fun save(view: View){
        val name = firstname.text.toString()
        val lastname = surname.text.toString()
        val Age = age.text.toString()
        val Email = email.text.toString()
        val Address = address.text.toString()
        val edit = sharedPreferences.edit()

        if(name.isNotEmpty() && lastname.isNotEmpty() && Age.isNotEmpty() && Email.isNotEmpty() && Address.isNotEmpty()
        ){
            edit.putString("firstname",name)
            edit.putString("lastname",lastname)
            edit.putString("email",Email)
            edit.putString("address",Address)
            edit.putString("age",Age)
            edit.apply()
            Toast.makeText(this, "saved", Toast.LENGTH_SHORT).show()
        }else{
            Toast.makeText(this, "Fill all fields", Toast.LENGTH_SHORT).show()
        }

    }

}